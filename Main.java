import java.util.ArrayList;
import java.util.Scanner;
// Aldo Kristiawan 235150401111009 Pemrograman Lanjut SI-A

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); // Membuat Scanner untuk mengambil input dari user
        ArrayList<Mahasiswa> daftarMahasiswa = new ArrayList<>(); // Buat ArrayList

        while (true) { // Loop untuk tambah mahasiswa
            System.out.println("Tambahkan mahasiswa? (y/t): ");
            String lanjut = scanner.nextLine(); // Mengambil keputusan pengguna apakah ingin menambah mahasiswa atau tidak
            if (!lanjut.equalsIgnoreCase("y")) { // Jika pengguna tidak ingin menambah, keluar dari loop
                break;
            }

            // Meminta input NIM dan nama dari user
            System.out.println("Masukkan NIM: ");
            String nim = scanner.nextLine();

            System.out.println("Masukkan nama: ");
            String nama = scanner.nextLine();

            Mahasiswa mahasiswaBaru = new Mahasiswa(nim, nama); // Membuat objek Mahasiswa baru
            daftarMahasiswa.add(mahasiswaBaru); // Menambahkan objek Mahasiswa ke dalam ArrayList

            while (true) { // Loop untuk menambahkan mata kuliah ke mahasiswa saat ini
                System.out.println("Tambahkan mata kuliah untuk " + nama + "? (y/t): ");
                lanjut = scanner.nextLine();
                if (!lanjut.equalsIgnoreCase("y")) { // Jika pengguna tidak ingin menambah, keluar dari loop
                    break;
                }

                // Meminta input kode mata kuliah, nama mata kuliah, dan nilai angka dari pengguna
                System.out.println("Masukkan kode mata kuliah: ");
                String kodeMK = scanner.nextLine();

                System.out.println("Masukkan nama mata kuliah: ");
                String namaMK = scanner.nextLine();

                System.out.println("Masukkan nilai (angka): ");
                int nilaiAngka = scanner.nextInt();
                scanner.nextLine();

                // Membuat objek MataKuliah baru
                MataKuliah mataKuliahBaru = new MataKuliah(kodeMK, namaMK, nilaiAngka);
                mahasiswaBaru.tambahMataKuliah(mataKuliahBaru); // Menambahkan objek MataKuliah ke daftar mata kuliah mahasiswa
            }
        }

        // Tampilkan KHS berisi setiap mahasiswa dan mata kuliahnya
        for (Mahasiswa mhs : daftarMahasiswa) {
            System.out.println("Mahasiswa: " + mhs.getNim() + " - " + mhs.getNama());
            System.out.println("Mata Kuliah:");
            for (MataKuliah mk : mhs.getMataKuliahList()) {
                // Cetak kode, nama mata kuliah, dan nilai huruf
                System.out.println(mk.getKodeMK() + " - " + mk.getNamaMK() + " - " + mk.getNilaiHuruf());
            }
            System.out.println();
        }

        scanner.close();
    }
}
