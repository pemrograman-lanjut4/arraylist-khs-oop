// Aldo Kristiawan 235150401111009 Pemrograman Lanjut SI-A
// Deklarasi atribut

public class MataKuliah {
    private String kodeMK;
    private String namaMK;
    private int nilaiAngka;

    // Konstruktor untuk kode mata kuliah, nama mata kulah, nilai angka
    public MataKuliah(String kodeMK, String namaMK, int nilaiAngka) {
        this.kodeMK = kodeMK;
        this.namaMK = namaMK;
        this.nilaiAngka = nilaiAngka;
    }

    // Getter untuk mendapatkan kode mata kuliah
    public String getKodeMK() {
        return kodeMK;
    }

    // Getter untuk mendapatkan nama mata kuliah
    public String getNamaMK() {
        return namaMK;
    }

    // Method cek nilai angka kemudian dikonversi menjadi huruf A-E
    public String getNilaiHuruf() {
        // Kondisi
        if (nilaiAngka >= 80) {
            return "A";
        } else if (nilaiAngka >= 60) {
            return "B";
        } else if (nilaiAngka >= 50) {
            return "C";
        } else if (nilaiAngka >= 40) {
            return "D";
        } else {
            return "E";
        }
    }
}
