import java.util.ArrayList;

// Aldo Kristiawan 235150401111009 Pemrograman Lanjut SI-A
// Deklarasi atribut
public class Mahasiswa {
    private String nim;
    private String nama;
    private ArrayList<MataKuliah> mataKuliahList;

    // Konstruktor untuk NIM dan nama
    public Mahasiswa(String nim, String nama) {
        this.nim = nim;
        this.nama = nama;
        this.mataKuliahList = new ArrayList<>();
    }

    // Method untuk nambah mata kuliah ke daftar mata kuliah mahasiswa
    public void tambahMataKuliah(MataKuliah mk) {
        mataKuliahList.add(mk);
    }

    // Getter untuk NIM mahasiswa
    public String getNim() {
        return nim;
    }

    // Getter untuk nama mahasiswa
    public String getNama() {
        return nama;
    }

    // Getter untuk mendapatkan daftar mata kuliah yang diambil mahasiswa
    public ArrayList<MataKuliah> getMataKuliahList() {
        return mataKuliahList;
    }
}
